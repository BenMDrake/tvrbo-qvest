// room script file

function hLRFrontDoor_Interact()
{
  if(!VapeFound){
    player.Say("I'm going to be a nervous wreck if I leave the house without my vape");
  }
  
  else{
    player.Walk(40, 120, eBlock);
    Game.StopAudio();
    player.ChangeRoom(2, 335, 102);
  }
}

function hStairs_Interact()
{
  player.Walk(350, 87, eBlock);
  player.ChangeRoom(3, 290, 135);
}

function hLRFrontDoor_MouseMove()
{
  mouse.Mode=eModeInteract;
}

function hWalkToKitchen_WalkOn()
{
  player.ChangeRoom(10, 10, 155);
}

function hStairs_Look()
{
  player.Say("The stairway. My room's up there.");
}

function hStairs_MouseMove()
{
  mouse.Mode=eModeInteract;
}


function hWalkToKitchen_Interact()
{
 player.Walk(107, 60);
}

function hRecordPlayer_Interact()
{
  player.Walk(165, 68,  eBlock);
  dRecordPlayer.Start();
}


function hRecordPlayer_MouseMove()
{
  mouse.SaveCursorUntilItLeaves();
  mouse.Mode=eModeInteract;
}

function hhGoldblum_MouseMove()
{
  mouse.SaveCursorUntilItLeaves();
  mouse.Mode=eModeLookat;
}

function hGoldblum_Look()
{
  player.Say("It's a portrait of Jeff Goldblum.");
  player.Say("I'm not entirely sure why it's in our living room.");
  if (cBen.Room == player.Room){
    cBen.Say("Because he's one of the finest actors of our generation.");
    cBen.Say("That's why.");
  }
}

function hCouch_MouseMove()
{
  mouse.SaveCursorUntilItLeaves();
  mouse.Mode=eModeLookat;
}

function hCouch_Look()
{
  player.Say("The red rocket. We found it on the side of the road.");
  player.Say("pretty good find. Only took a few weeks worth of febreezing.");
}

function HCouchCush_MouseMove()
{
  mouse.SaveCursorUntilItLeaves();
  mouse.Mode=eModeInteract;
}

function HCouchCush_Interact()
{
  player.Walk(185, 165,  eBlock);
  player.LockView(15);
  player.Animate(0, 3);
  player.UnlockView();
  if (!CouchPenniesFound){
    oChange.Visible = true;
    player.Say("Oh hey! Couch pennies.");
    CouchPenniesFound = true;
  }
  else if (CouchPenniesFound){
    player.Say("Nuts, nothing left in here but dust and pop tart crumbs.");
  }
}

function oChange_Interact()
{
  player.InventoryQuantity[iCash.ID] += 1;
  oChange.Visible = false;
  Display("you picked up $1. You now have $%d.", player.InventoryQuantity[iCash.ID]);
}

function hBricksDoor_MouseMove()
{
  mouse.SaveCursorUntilItLeaves();
  mouse.Mode=eModeLookat;
}

function hBricksDoor_Look()
{
  player.Say("Bricks room.");
  player.Say("It's.. uh... quarantined.. right now.");
}
